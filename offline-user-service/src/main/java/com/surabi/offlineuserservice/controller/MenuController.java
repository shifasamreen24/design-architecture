package com.surabi.offlineuserservice.controller;

import com.surabi.offlineuserservice.entity.EventEntity;
import com.surabi.offlineuserservice.entity.MenuEntity;
import com.surabi.offlineuserservice.entity.OrderEntity;
import com.surabi.offlineuserservice.exception.RestaurantCustomException;
import com.surabi.offlineuserservice.model.Feedback;
import com.surabi.offlineuserservice.model.Mode;
import com.surabi.offlineuserservice.model.Status;
import com.surabi.offlineuserservice.repository.EventRepository;
import com.surabi.offlineuserservice.repository.MenuRepository;
import com.surabi.offlineuserservice.repository.OrderRepository;
import com.surabi.offlineuserservice.service.IMenuService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.jni.Local;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Slf4j
@RestController
@RequestMapping("/api/v1")
public class MenuController {

    @Autowired
    IMenuService iMenuService;
    @Autowired
    MenuRepository menuRepository;
    @Autowired
    OrderRepository orderRepository;
    @Autowired
    EventRepository eventRepository;

    //get all menu items
    @GetMapping(value = "/menu")
    @ApiOperation(value = "Get All Menu items", notes = "Fetch all items from menu as list")
    public List<MenuEntity> getAllMenuItems() throws Exception{
        return menuRepository.findAll();
    }

    //select n items n times to get the bill and create and unpaid order id with unpaid status with other details.
    @PostMapping("/generate_bill")
    @ApiOperation(value = "Select n items n times", notes = "Generate bill for selected items")
    public Map<String, String> selectMenuAndGenerateBill(@RequestBody HashMap<Long, Integer> items) throws Exception{
        Double total_bill_amount = 0.0;
        for(Map.Entry<Long, Integer> item: items.entrySet()){
            total_bill_amount += (menuRepository.getMenuItemPrice(item.getKey()))*(item.getValue());
        }
        //insert values into order table. -> order id, bill amount generated, items map, paid status, payment mode
        OrderEntity orderDto = new OrderEntity();
        orderDto.setId(UUID.randomUUID().toString());
        orderDto.setBill(total_bill_amount);
        orderDto.setItems(items.toString());
        orderDto.setStatus(Status.UNPAID);
        orderDto.setMode(Mode.CARD);
        orderRepository.save(orderDto);
        Map<String, String> unpaidOrderDetails = new HashMap<>();
        unpaidOrderDetails.put("Bill amount to pay",String.valueOf(total_bill_amount));
        unpaidOrderDetails.put("Order id for payment", orderDto.getId());
        return unpaidOrderDetails;
    }

    //give feedback
    @PatchMapping("/provideFeedback/{orderId}")
    @ApiOperation(value= "Provide Order Feedback", notes = "Please rate on a scale from 0-10 with your comments")
    public void provideOrderFeedback(@PathVariable String orderId, @Valid @RequestBody Feedback feedback) throws RestaurantCustomException{
        Optional<OrderEntity> currentOrder =  orderRepository.findById(orderId);
        if(currentOrder.isPresent()) {
            try {
                OrderEntity order = currentOrder.get();
                order.setFeedback(feedback.toString());
                orderRepository.saveAndFlush(order);
            }
            catch (Exception ex){
                throw new RestaurantCustomException("Order feedback update execution failed", HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        else {
            throw new RestaurantCustomException("Order does not exist for given id ", HttpStatus.NOT_FOUND);
        }
    }

    //payment using order id
    @PatchMapping("/order_payment")
    @ApiOperation(value = "Pay by order id using cash or card",notes = "Get the details of bill before payment")
    public String orderPayment(@RequestParam String orderId, @RequestParam Mode mode) throws RestaurantCustomException{
        Optional<OrderEntity> currentOrder =  orderRepository.findById(orderId);
        StringBuilder billDetails = new StringBuilder();
        if(currentOrder.isPresent()) {
            try {
                OrderEntity order = currentOrder.get();
                order.setMode(mode);
                order.setStatus(Status.PAID);
                orderRepository.saveAndFlush(order);
                billDetails.append("Total bill amount to pay:- ");
                billDetails.append(order.getBill().toString());
                billDetails.append("\n");
                billDetails.append("Items ordered:- ");
                billDetails.append(order.getItems());
            }
            catch (Exception ex){
                throw new RestaurantCustomException("Order payment failed, please try again", HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        else {
            throw new RestaurantCustomException("Order does not exist for given id ", HttpStatus.NOT_FOUND);
        }
        return billDetails.toString();
    }

    @PatchMapping("/advance_booking")
    @ApiOperation(value = "Book a seat at least 2 days before the event",notes = "Confirm payment by event date, order id and mode of payment")
    public String advanceEventBooking(@RequestParam @DateTimeFormat(pattern = "dd/MM/yyyy") LocalDate eventDate, @RequestParam String orderId, @RequestParam Mode mode) throws RestaurantCustomException{

        //generate bill
        // check if order exists
        Optional<OrderEntity> currentOrder =  orderRepository.findById(orderId);
        Optional<EventEntity> currentEvent = eventRepository.findByDate(eventDate);
        Date today = new Date();
        // check event availability
        LocalDate todayDate = LocalDate.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/YYYY");
        Period period = Period.between(todayDate, eventDate);
        if(period.getDays()<2){
            throw new RestaurantCustomException("Please book at least 2 days in advance", HttpStatus.BAD_REQUEST);
        };
        StringBuilder billDetails = new StringBuilder();
        // if both order and event exists
        if(currentOrder.isPresent() && currentEvent.isPresent()) {
            try{
                //update payment status, mode and event date in given order
                OrderEntity order = currentOrder.get();
                order.setMode(mode);
                order.setStatus(Status.PAID);
                order.setEvent(eventDate);
                orderRepository.saveAndFlush(order);
                billDetails.append("Booking confirmed! Total bill amount payed:- ");
                billDetails.append(order.getBill().toString());
                billDetails.append("\n");
                billDetails.append("Items ordered:- ");
                billDetails.append(order.getItems());
            }
            catch (Exception ex){
                throw new RestaurantCustomException("Payment failed. Please try again!", HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        else {
            throw new RestaurantCustomException("Either the order or event doe snot exist", HttpStatus.NOT_FOUND);
        }
        return billDetails.toString();
    }
}
