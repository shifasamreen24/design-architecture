insert into menu_entity values(1, 'item1',100.00);
insert into menu_entity values(2, 'item2',200.00);
insert into menu_entity values(3, 'item3',300.00);

insert into event_entity(id, name, date) values ( 1, 'event1', PARSEDATETIME('14/11/2021','dd/MM/yyyy'));
insert into event_entity(id, name, date) values ( 2, 'event2',PARSEDATETIME('30/11/2021','dd/MM/yyyy'));
insert into event_entity(id, name, date) values ( 3, 'event3',PARSEDATETIME('1/12/2021','dd/MM/yyyy'));

insert into order_entity(id, bill, event, feedback, items, mode, status) values ('f0818cb4-5e4a-49b8-9ecb-ef35c269d98a',	100.0,	null,	null,	'{1=1, 2=0, 3=0}',	'CARD', 'UNPAID')
